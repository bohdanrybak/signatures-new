package com.ck.chnu.signatures.dataProcessing;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.error.MeanSquaredError;
import org.neuroph.nnet.ConvolutionalNetwork;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.ConvolutionalBackpropagation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class NeuralNetwork {

    private ConvolutionalNetwork network;
    private ConvolutionalBackpropagation backPropagation;

    public static final int TYPE_NETWORK_RECOGNITION = 0;
    public static final int TYPE_NETWORK_CHARACTER = 1;

    public int type;

    public NeuralNetwork(int countOfOutputs, int type) {

        this.type = type;

        try {
            if (type == TYPE_NETWORK_RECOGNITION) {
                FileInputStream stream = new FileInputStream("recognitionNetwork.nnet");
                network = (ConvolutionalNetwork) ConvolutionalNetwork.load(stream);
            } else if (type == TYPE_NETWORK_CHARACTER) {
                FileInputStream stream = new FileInputStream("characterNetwork.nnet");
                network = (ConvolutionalNetwork) ConvolutionalNetwork.load(stream);
            }
        } catch (FileNotFoundException e) {
           initNetwork(countOfOutputs);
        }
    }

    public void initNetwork(int countOfOutputs){
        network =
                new ConvolutionalNetwork.Builder()
                        .withInputLayer(80, 40, 1)
                        .withConvolutionLayer(4, 4, 15)
                        .withConvolutionLayer(4, 4, 7)
                        .withFullConnectedLayer(countOfOutputs)
                        .build();
        backPropagation = new ConvolutionalBackpropagation();
        backPropagation.setLearningRate(0.2);
        backPropagation.setMaxError(0.01);
        backPropagation.setMaxIterations(20);
        backPropagation.setErrorFunction(new MeanSquaredError());
        network.randomizeWeights();
        network.setLearningRule(backPropagation);
    }

    public void learnNetwork(DataSet dataSet) {
        network.reset();
        backPropagation.addListener(new LearningListener());
        network.learn(dataSet);
//        switch (type){
//            case TYPE_NETWORK_RECOGNITION:
//                network.save("recognitionNetwork.nnet");
//                break;
//            case TYPE_NETWORK_CHARACTER:
//                network.save("characterNetwork.nnet");
//        }
    }

    public double[] useNetwork(DataSet dataSet) {
        network.setInput(dataSet.getRowAt(0).getInput());
        network.calculate();
        return network.getOutput();
    }

    private static class LearningListener implements LearningEventListener {

        long start = System.currentTimeMillis();


        public void handleLearningEvent(LearningEvent event) {
            BackPropagation bp = (BackPropagation) event.getSource();
            System.out.println("Iteration: " + bp.getCurrentIteration() + " Error: " + bp.getTotalNetworkError());
            System.out.println("Epoch execution time: " + (System.currentTimeMillis() - start) / 1000.0 + " sec");

            start = System.currentTimeMillis();
        }

    }
}
