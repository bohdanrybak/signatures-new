package com.ck.chnu.signatures.dataProcessing;

import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.util.List;

public class NeuralNetwork_dl4j {
    public static final int TYPE_NETWORK_RECOGNITION = 0;
    public static final int TYPE_NETWORK_CHARACTER = 1;

    public int type;
    public int countOfOutputs;

    private MultiLayerNetwork network;

    public NeuralNetwork_dl4j(int countOfOutputs, int type) {
        this.type = type;
        this.countOfOutputs = countOfOutputs;

        try {
            if (type == TYPE_NETWORK_RECOGNITION) {
                network = ModelSerializer.restoreMultiLayerNetwork("recognition_network.neu");
            } else if (type == TYPE_NETWORK_CHARACTER) {
                network = ModelSerializer.restoreMultiLayerNetwork("character_network.neu");
            }
        } catch (Exception e) {
            MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                    .seed(System.currentTimeMillis())
                    .l2(0.0005)
                    .weightInit(WeightInit.XAVIER)
                    .updater(new Nesterovs(0.01, 0.9))
                    .list()
                    .layer(0, new ConvolutionLayer
                            .Builder(4, 4)
                            .nIn(1)
                            .stride(1, 1)
                            .nOut(20)
                            .activation(Activation.TANH)
                            .build()
                    )
                    .layer(1, new ConvolutionLayer
                            .Builder(4, 4)
                            .stride(1, 1)
                            .nOut(16)
                            .activation(Activation.TANH)
                            .build()
                    )
                    .layer(2, new ConvolutionLayer
                            .Builder(4, 4)
                            .stride(1, 1)
                            .nOut(8)
                            .activation(Activation.TANH)
                            .build()
                    )
                    .layer(3, new DenseLayer
                            .Builder()
                            .activation(Activation.RELU)
                            .nOut(100)
                            .build())
                    .layer(4, new OutputLayer
                            .Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                            .nOut(countOfOutputs)
                            .activation(Activation.SOFTMAX)
                            .build())
                    .setInputType(InputType.convolutionalFlat(40, 80, 1))
                    .backprop(true)
                    .pretrain(false)
                    .build();
            network = new MultiLayerNetwork(conf);
//            try {
//                switch (type) {
//                    case TYPE_NETWORK_RECOGNITION:
//                        ModelSerializer.writeModel(network, "recognition_network.neu", true);
//                        break;
//                    case TYPE_NETWORK_CHARACTER:
//                        ModelSerializer.writeModel(network, "character_network.neu", true);
//                        break;
//                }
//            } catch (IOException ex) {
//                System.out.println(ex.getMessage());
//            }
        }
    }

    public void learnNetwork(List<DataSet> sets, int epochCount) {
        network.setListeners(new ScoreIterationListener(1));
        for (int i = 0; i < epochCount; i++) {
            for (DataSet set : sets) {
                network.fit(set);
            }
        }
    }

    public INDArray useNetwork(INDArray input) {
        network.activate(0, input);
        network.activate(1);
        network.activate(2);
        network.activate(3);
        return network.getOutputLayer().activate();
    }
}
