package com.ck.chnu.signatures.tools;

import com.ck.chnu.signatures.data.entity.Signature;
import com.ck.chnu.signatures.data.entity.User;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataSetGenerator {
    private static DataSetGenerator instance;

    private DataSetGenerator() {
    }

    public static DataSetGenerator getInstance() {
        if (instance == null) {
            instance = new DataSetGenerator();
        }
        return instance;
    }

    public org.neuroph.core.data.DataSet generateUsersTrainDataSet_neuroph(List<User> users) throws IOException {
        org.neuroph.core.data.DataSet res = new org.neuroph.core.data.DataSet(3200,users.size());
        for(User u : users){
            double[] output = generateTrainUserOutput(u, users.size());
            for(Signature s : u.signatures){
                ByteArrayInputStream bais = new ByteArrayInputStream(s.image);
                BufferedImage img = ImageIO.read(bais);
                res.addRow(ArraysTransformer.getInstance().fromMatrixToArray(ImageTransformer.getInstance().transformImage(img)), output);
            }
        }
        return res;
    }

    public org.neuroph.core.data.DataSet generateUsingDataSet_neuroph(Signature sign) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(sign.image);
        BufferedImage img = ImageIO.read(bais);
        org.neuroph.core.data.DataSet res = new org.neuroph.core.data.DataSet(3200);
        res.addRow(ArraysTransformer.getInstance().fromMatrixToArray(ImageTransformer.getInstance().transformImage(img)));
        return res;
    }

    public INDArray generateUsingData_dl4j(Signature sign) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(sign.image);
        BufferedImage img = ImageIO.read(bais);
        return Nd4j.create(
                ArraysTransformer.getInstance()
                        .fromMatrixToArray(ImageTransformer.getInstance()
                                .transformImage(img)));
    }

    public List<DataSet> generateUsersTrainDataSet_dl4j(Iterable<User> users) throws IOException {
        List<User> usersCollection = new ArrayList<>();
        users.forEach(usersCollection::add);

        List<DataSet> sets = new ArrayList<>();

        for(User u : usersCollection){
            double[] output = generateTrainUserOutput(u, usersCollection.size());
            for(Signature s : u.signatures){
                ByteArrayInputStream bais = new ByteArrayInputStream(s.image);
                BufferedImage img = ImageIO.read(bais);
                sets.add(new DataSet(
                        Nd4j.create(ArraysTransformer
                                .getInstance()
                                .fromMatrixToArray(ImageTransformer.getInstance().transformImage(img))),
                        Nd4j.create(output))
                );
            }
        }
        return sets;
    }

    private double[] generateTrainUserOutput(User user, int usersCount){
        double[] res = new double[usersCount];

        for(int i=0;i<res.length;i++){
            if(i == user.id){
                res[i] = 1;
            }
            else {
                res[i] = 0;
            }
        }
        return res;
    }
}
