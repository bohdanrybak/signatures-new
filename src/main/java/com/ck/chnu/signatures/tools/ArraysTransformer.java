package com.ck.chnu.signatures.tools;

import java.util.ArrayList;
import java.util.List;

public class ArraysTransformer {

    private static ArraysTransformer instance;

    private ArraysTransformer() {
    }

    public static ArraysTransformer getInstance() {
        if (instance == null) {
            instance = new ArraysTransformer();
        }
        return instance;
    }

    public double[] fromMatrixToArray(List<List<Double>> matrix) {
        List<Double> resList = new ArrayList<>();
        for (List<Double> list : matrix) {
            resList.addAll(list);
        }
        double[] array = new double[resList.size()];
        for(int i = 0; i < resList.size();i++){
            array[i] = resList.get(i);
        }
        return array;
    }
}
