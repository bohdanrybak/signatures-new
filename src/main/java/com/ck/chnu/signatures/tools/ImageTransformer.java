package com.ck.chnu.signatures.tools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageTransformer {

    int counter = 0;

    private static ImageTransformer instance;

    private ImageTransformer() {
    }

    public static ImageTransformer getInstance() {
        if (instance == null) {
            instance = new ImageTransformer();
        }
        return instance;
    }

    public List<List<Double>> transformImage(BufferedImage img) throws IOException {
        //makeGray(img);
        img = changeBrightness(img, 20.0f);
        List<List<Double>> res = makeWhiteAndBlack(img);
        File outputfile = new File(counter + ".jpg");
        ImageIO.write(img, "jpg", outputfile);
        counter++;
        return res;
    }

    public void makeGray(BufferedImage img) {
        for (int x = 0; x < img.getWidth(); ++x)
            for (int y = 0; y < img.getHeight(); ++y) {
                int rgb = img.getRGB(x, y);
                int r = (rgb >> 16) & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb & 0xFF);

                int grayLevel = (r + g + b) / 3;
                int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel;
                img.setRGB(x, y, gray);
            }
    }

    private BufferedImage changeBrightness(BufferedImage src, float val){
        RescaleOp brighterOp = new RescaleOp(val, 0, null);
        return brighterOp.filter(src,null);
    }

    private List<List<Double>> makeWhiteAndBlack(BufferedImage img) {
        List<List<Double>> binaryImage = new ArrayList<>();
        for (int i = 0; i < img.getHeight(); i++) {
            binaryImage.add(new ArrayList<>());
            for (int j = 0; j < img.getWidth(); j++) {
                Color color = new Color(img.getRGB(j, i));
                float luminance = (color.getRed() * 0.2126f + color.getGreen() * 0.7152f + color.getBlue() * 0.0722f) / 255;
                if (luminance >= 0.7f) {
                    binaryImage.get(i).add(0.0);
                    Color black = new Color(0, 0, 0);
                    img.setRGB(j, i, black.getRGB());
                } else {
                    binaryImage.get(i).add(1.0);
                    Color white = new Color(255, 255, 255);
                    img.setRGB(j, i, white.getRGB());
                }
            }
        }

        return binaryImage;
    }

}
