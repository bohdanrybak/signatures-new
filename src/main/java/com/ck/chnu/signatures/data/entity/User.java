package com.ck.chnu.signatures.data.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;


@Entity
public class User {
    @Id
    public long id;

    public String firstName;
    public String lastName;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    public List<Signature> signatures = new ArrayList<>();
}
