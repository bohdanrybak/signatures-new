package com.ck.chnu.signatures.data.entity.network;

public class SuccessResult {
    private String message;

    public SuccessResult() {
        this.message = "Success";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
