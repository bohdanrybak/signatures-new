package com.ck.chnu.signatures.data.repository;

import com.ck.chnu.signatures.data.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{}
