package com.ck.chnu.signatures.data.entity.network;

import java.util.List;

public class AddUserRequest {
    private String firstName;
    private String lastName;

    private List<String> images;

    public AddUserRequest(String firstName, String lastName, List<String> images) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.images = images;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
