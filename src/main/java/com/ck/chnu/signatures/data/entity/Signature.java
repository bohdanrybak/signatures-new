package com.ck.chnu.signatures.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Signature {
    @GeneratedValue
    @Id
    public Long id;

    @Lob
    public byte[] image;

    public Signature(byte[] image) {
        this.image = image;
    }

    public Signature() {
    }
}
