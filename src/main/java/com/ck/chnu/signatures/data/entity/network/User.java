package com.ck.chnu.signatures.data.entity.network;

public class User {
    private String firstName;
    private String lastName;
    private double percenage;
    private String charackter;

    public User(String firstName, String lastName, double percenage, String charackter) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.percenage = percenage;
        this.charackter = charackter;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getPercenage() {
        return percenage;
    }

    public void setPercenage(double percenage) {
        this.percenage = percenage;
    }

    public String getCharackter() {
        return charackter;
    }

    public void setCharackter(String charackter) {
        this.charackter = charackter;
    }
}
