package com.ck.chnu.signatures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SignaturesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SignaturesApplication.class, args);
	}
}
