package com.ck.chnu.signatures.api;

import com.ck.chnu.signatures.data.entity.Signature;
import com.ck.chnu.signatures.data.entity.User;
import com.ck.chnu.signatures.data.entity.network.SuccessResult;
import com.ck.chnu.signatures.data.repository.UserRepository;
import com.ck.chnu.signatures.dataProcessing.NeuralNetwork;
import com.ck.chnu.signatures.tools.DataSetGenerator;
import com.google.gson.GsonBuilder;
import org.neuroph.core.data.DataSet;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ApiController {

    NeuralNetwork recognitionNetwork;

    private final UserRepository userRepository;

    public ApiController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("api/find_people")
    public String findPeople(@RequestParam("signature") MultipartFile signature) throws IOException {
        Signature sign = new Signature(signature.getBytes());
        double[] res = recognitionNetwork.useNetwork(DataSetGenerator.getInstance().generateUsingDataSet_neuroph(sign));
        int maxIndex = 0;
        for(int i=1;i<res.length;i++){
            if(res[i] > res[maxIndex]){
                maxIndex = i;
            }
        }
        Optional<User> currUser = userRepository.findById((long) maxIndex);
        User userInstance = currUser.get();
        com.ck.chnu.signatures.data.entity.network.User resUser = new com.ck.chnu.signatures.data.entity.network.User(
                userInstance.firstName,
                userInstance.lastName,
                res[maxIndex],
                "Добрий"
        );
        return new GsonBuilder().create().toJson(resUser);
    }

    @PostMapping("api/add_people")
    public String addPeople(@RequestParam(name = "firstName") String firstName,
                            @RequestParam(name = "lastName") String lastName,
                            @RequestParam(name = "signature1") MultipartFile signature1,
                            @RequestParam(name = "signature2") MultipartFile signature2,
                            @RequestParam(name = "signature3") MultipartFile signature3,
                            @RequestParam(name = "signature4") MultipartFile signature4,
                            @RequestParam(name = "signature5") MultipartFile signature5
    ) throws IOException {

        User user = new User();
        user.firstName = firstName;
        user.lastName = lastName;
        user.id = userRepository.count();

        List<Signature> userSignatures = new ArrayList<>();

        userSignatures.add(new Signature(signature1.getBytes()));
        userSignatures.add(new Signature(signature2.getBytes()));
        userSignatures.add(new Signature(signature3.getBytes()));
        userSignatures.add(new Signature(signature4.getBytes()));
        userSignatures.add(new Signature(signature5.getBytes()));

        user.signatures = userSignatures;

        userRepository.save(user);

        Iterable<User> allUsers = userRepository.findAll();
        List<User> usersCollection = new ArrayList<>();
        allUsers.forEach(usersCollection::add);

        DataSet trainDataSet = DataSetGenerator.getInstance().generateUsersTrainDataSet_neuroph(usersCollection);
        recognitionNetwork = new NeuralNetwork(usersCollection.size(), NeuralNetwork.TYPE_NETWORK_RECOGNITION);

        recognitionNetwork.initNetwork(usersCollection.size());

        recognitionNetwork.learnNetwork(trainDataSet);


        return new GsonBuilder().create().toJson(new SuccessResult());
    }

    @GetMapping("/ping")
    public String ping(){
        return "pong";
    }
}
